const mongoose = require('mongoose')

const adSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: {type:String, required:true, minlength: 2}, //ad title
    adOwner: {type:String, required:true, minlength: 2}, //ad owner
    repetition: {type: Number,required: true}, //how many times should this ad be repeated
    counter: {type: Number,required: true}, //how many times did this ad actually repeated. this counters starts with 0
    video: {type:String, required:true} //path to the video ad

});

module.exports = mongoose.model('Ad', adSchema)
