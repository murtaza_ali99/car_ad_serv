const express = require('express');
const router = express.Router();
const mongoose = require('mongoose')
const Ad = require('../modules/ads')
const uuidv1 = require('uuid/v1');
const fs = require('fs');


// admin
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzMwZjYxYzc3ODI0ODI2N2M1MmM3NzgiLCJpYXQiOjE1NDY3MTI2MDR9.ePgQC7a7Pblotis7h2lG8dnM_HzSU5khD_dgsh6A328

//some user
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzMwZjY3NTA0Mzk5NTJlMzhkODY5Y2IiLCJpYXQiOjE1NDY3MTI2OTN9.J81nK5F7r3X7fz_7aa2PSRnOIrIS5wPyCDFKiDTkNhc

// Getting all adss
router.get('/', (req, res) => {

  Ad.find().then(result => {
    res.send(result)
  }).catch(err => {
    res.status(404).send(err)
  })

});


//posting a new ad
router.post('/', (req, res) => {
  console.log(req.body)
  console.log(req.files)

  let video = req.files.file;
  videoName = uuidv1();


  video.mv(`./public/${videoName}.mp4`, function (err) {

    const ad = new Ad({
      _id: new mongoose.Types.ObjectId(),
      title: req.body.title,
      adOwner: req.body.owner,
      repetition: req.body.repeat,
      counter: 0,
      video: `${videoName}.mp4`

    })
    ad.save()
    res.send('Ad Saved');

  });

});


//deleting a ad
router.delete('/:id', (req, res) => {

  // find the item we want to delete
  Ad.findById(req.params.id).then((result) => {
    // delete the video file from the server
    fs.exists(`./public/${result.video}`, (exists) => {
      if (exists) {
        fs.unlink(`./public/${result.video}`, (err) => {

          if (err) throw err;
          console.log('file successfully deleted');

          // delete all the othe info from the database
          Ad.remove({ _id: req.params.id }).then(result => {
            res.send('item deleted :(');
          }).catch(errr => {
            res.status(404).send(errr);
          });
        });
      } else {
        Ad.remove({ _id: req.params.id }).then(result => {
          res.send('item deleted :(');
        }).catch(errr => {
          res.status(404).send(errr);
        });
      }
    });
});
});


//updating ad
router.put('/:id', (req, res) => {

  console.log(req.body)

  // to update only string informations

  Ad.findByIdAndUpdate(req.body.ad._id, req.body.ad)
    .then(result => {
      res.send('Information updated');
    }).catch(errr => {
      res.status(400).send(errr);
    });



  //to update video file and string informations

  // //find the item we want to update
  // Ad.findById(req.params.id).then(result => {

  //     //get how many times did this ad actualy repeated
  //     let counter = result.counter

  //     // delete the old video file from the server to create new one
  //     fs.unlink(result.video, (error) => {

  //         // if (error) throw error;
  //         console.log(error)
  //         console.log('old file is deleted to create new one');

  //         //create new video
  //         let video = req.files.vid;
  //         videoName = uuidv1();

  //         //move the new uploade file to (public)
  //         video.mv(`./public/${videoName}.mp4`, function(er) {

  //             //update the other information in the database
  //             Ad.update({_id: req.params.id},
  //                 {$set:{title:  req.body.title,
  //                      adOwner:  req.body.owner,
  //                      repetition:  req.body.repeat,
  //                      counter:  counter,
  //                      video:  `./public/${videoName}.mp4`
  //                 }})
  //               .then(result => {
  //                 res.send('Information updated');
  //               }).catch(errr => {
  //                 res.status(400).send(errr);
  //               });
  //           });

  //     });
  //   }).catch(errror => {
  //     res.status(400).send(errror.message)
  // });
});



module.exports = router;
