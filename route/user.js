const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const User = require('../modules/user');
const Joi = require('joi');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');


//display the user information

router.get('/', (req, res) => {
  let colection=[]
  User.find().then(result => {
    result.map(re=>{
      if(re.admin==false){
        colection.push({_id:re._id,name:re.name ,email:re.email,admin:re.admin})
      }
    
    })
    res.send(colection)
  }).catch(err => {
    res.status(404).send(err)
  })
});


// register new user
router.post('/register', (req, res) => {
  console.log("hello from register")
  bcrypt.genSalt(10).then(salt => {
    bcrypt.hash(req.body.password, salt).then(hashed => {
      const user = new User({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        password: hashed,
        email: req.body.email,
        admin: req.body.admin || false
      });
      user.save().then(result => {
        const token = jwt.sign({ _id: result._id }, 'sweet_koko');
        res.header({ 'x-auth-token': token }).send('New user has been added');
      }).catch(err => {
        res.send(err);
      })
    })
  });
});



//login 


router.post('/login', (req, res) => {
  //  check if there is a user data (username & password) in the req body
  const validating = userValidating(req.body);
  if (validating.error) {
    res.status(400).send(validating.error);
  } else {
    //  chekc if there is such email get the user info
    User.findOne({ email: req.body.email })
      .then(result => {
        const admin=result.admin
        //  check if the password valid
        bcrypt.compare(req.body.password, result.password, function (err, response) {
          if (response) {
            //  create a new token and send it back to the user in the response header
            const token = jwt.sign({ "_id": result._id }, 'sweet_koko');
            res.send({token});
          } else {
            res.status(400).send('you have tried an incorect credentials');
          }
        });
      }).catch(err => {
        res.status(404).send('there is no such user');
      });
  }

});

//to delet user by using email

router.delete('/delet/:email', (req, res) => {
  User.remove({ email: req.params.email }).then(result => {
    res.send(`Number of deleted users is ${result.n}`)
  }).catch(err => {
    res.status(400).send(err);
  });
});


//  VALIDATION

function userValidating(user) {
  const userSchema = {
    'email': Joi.string().min(3).required(),
    'password': Joi.string().min(2).required()
  };
  return Joi.validate(user, userSchema);
}

module.exports = router;
