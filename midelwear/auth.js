const jwt = require('jsonwebtoken');
const User = require('../modules/user')

function auth(req, res, next) {
    //  check if there is a token
    const token = req.headers.token;
    if(token){
    //  decode the token and chekc if it's validate
      try {
        //  Get the payload from the jsonwebtoken
        let payload = jwt.verify(token, 'sweet_koko');
        
        //check if this user is the admin
        let user = User.findById(payload._id).then(result => {
            if(result.admin == true){ 
              next()
            }else{
              res.status(404).send('you are not the admin');
            }

          }).catch(err => {
            res.status(404).send(err);
          })

      } catch (err) {
        res.status(400).send('you are not the admin');
      }
    }else{
      res.send('you need to login');
    }
  }
  
  
  module.exports = auth;
  