const express = require('express');
const app = express();
const mongoose = require('mongoose');
const fileUpload = require('express-fileupload');
const usersRoutes= require('./route/user')
const adsRoutes= require('./route/ads')
const auth =require('./midelwear/auth')
const cors = require('cors');


//this to connect with database dy using mongo db
mongoose.connect('mongodb://admin:admin123456@ds149414.mlab.com:49414/car_ad', { useNewUrlParser: true });

mongoose.connection.on('connected', () => {
    console.log('\x1b[36m%s\x1b[0m', 'mongo has been connected...');
  });
//to fix some error
  //you can use this 
  // app.use(function(req, res, next) {
  //   res.header("Access-Control-Allow-Origin", "*");
  //   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  //   res.header("Access-Control-Allow-Methods", "GET, PUT, POST ,DELETE");
  //   next();
  // });
  
  //or this
   app.use(cors());


  // this to upload files to my server
  app.use(fileUpload());
  app.use(express.static('public'))
  app.use(express.json());
  app.use('/api/user', usersRoutes);
  app.use('/api/ad', adsRoutes);

//this to test my server is work
  app.get('/', (req, res) => {

    res.send("hello");
  });

  app.get('/api/files/:file', (req, res)=> {
    res.sendFile(__dirname + '/public/' + req.params.file);
  })



  //this to run my server on port 5000
  app.listen("5000", () => {
    console.log("Running on port 5000");
  });